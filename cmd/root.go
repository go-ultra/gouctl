package cmd

import (
	"fmt"
	"os"
	"runtime"
	"strings"

	"gitee.com/go-ultra/gouctl/api"
	"gitee.com/go-ultra/gouctl/bug"
	"gitee.com/go-ultra/gouctl/docker"
	"gitee.com/go-ultra/gouctl/env"
	"gitee.com/go-ultra/gouctl/internal/version"
	"gitee.com/go-ultra/gouctl/kube"
	"gitee.com/go-ultra/gouctl/migrate"
	"gitee.com/go-ultra/gouctl/model"
	"gitee.com/go-ultra/gouctl/quickstart"
	"gitee.com/go-ultra/gouctl/rpc"
	"gitee.com/go-ultra/gouctl/tpl"
	"gitee.com/go-ultra/gouctl/upgrade"
	"github.com/logrusorgru/aurora"
	"github.com/spf13/cobra"
)

const (
	codeFailure = 1
	dash        = "-"
	doubleDash  = "--"
	assign      = "="
)

var rootCmd = &cobra.Command{
	Use:   "goctl",
	Short: "A cli tool to generate go-zero code",
	Long:  "A cli tool to generate api, zrpc, model code",
}

// Execute executes the given command
func Execute() {
	os.Args = supportGoStdFlag(os.Args)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(aurora.Red(err.Error()))
		os.Exit(codeFailure)
	}
}

func supportGoStdFlag(args []string) []string {
	copyArgs := append([]string(nil), args...)
	parentCmd, _, err := rootCmd.Traverse(args[:1])
	if err != nil { // ignore it to let cobra handle the error.
		return copyArgs
	}

	for idx, arg := range copyArgs[0:] {
		parentCmd, _, err = parentCmd.Traverse([]string{arg})
		if err != nil { // ignore it to let cobra handle the error.
			break
		}
		if !strings.HasPrefix(arg, dash) {
			continue
		}

		flagExpr := strings.TrimPrefix(arg, doubleDash)
		flagExpr = strings.TrimPrefix(flagExpr, dash)
		flagName, flagValue := flagExpr, ""
		assignIndex := strings.Index(flagExpr, assign)
		if assignIndex > 0 {
			flagName = flagExpr[:assignIndex]
			flagValue = flagExpr[assignIndex:]
		}

		f := parentCmd.Flag(flagName)
		if f == nil {
			continue
		}
		if f.Shorthand == flagName {
			continue
		}

		goStyleFlag := doubleDash + f.Name
		if assignIndex > 0 {
			goStyleFlag += flagValue
		}

		copyArgs[idx] = goStyleFlag
	}
	return copyArgs
}

func init() {
	rootCmd.Version = fmt.Sprintf("%s %s/%s", version.BuildVersion,
		runtime.GOOS, runtime.GOARCH)
	rootCmd.AddCommand(api.Cmd)
	rootCmd.AddCommand(bug.Cmd)
	rootCmd.AddCommand(docker.Cmd)
	rootCmd.AddCommand(kube.Cmd)
	rootCmd.AddCommand(env.Cmd)
	rootCmd.AddCommand(model.Cmd)
	rootCmd.AddCommand(migrate.Cmd)
	rootCmd.AddCommand(quickstart.Cmd)
	rootCmd.AddCommand(rpc.Cmd)
	rootCmd.AddCommand(tpl.Cmd)
	rootCmd.AddCommand(upgrade.Cmd)
}
