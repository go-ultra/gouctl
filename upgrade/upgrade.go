package upgrade

import (
	"fmt"
	"runtime"

	"gitee.com/go-ultra/gouctl/rpc/execx"
	"github.com/spf13/cobra"
)

// Upgrade gets the latest goctl by
// go install gitee.com/go-ultra/gouctl/goctl@latest
func upgrade(_ *cobra.Command, _ []string) error {
	cmd := `GO111MODULE=on GOPROXY=https://goproxy.cn/,direct go install gitee.com/go-ultra/gouctl/goctl@latest`
	if runtime.GOOS == "windows" {
		cmd = `set GOPROXY=https://goproxy.cn,direct && go install gitee.com/go-ultra/gouctl/goctl@latest`
	}
	info, err := execx.Run(cmd, "")
	if err != nil {
		return err
	}

	fmt.Print(info)
	return nil
}
