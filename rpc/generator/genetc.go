package generator

import (
	_ "embed"
	"fmt"
	"path/filepath"
	"strings"

	conf "gitee.com/go-ultra/gouctl/config"
	"gitee.com/go-ultra/gouctl/rpc/parser"
	"gitee.com/go-ultra/gouctl/util"
	"gitee.com/go-ultra/gouctl/util/format"
	"gitee.com/go-ultra/gouctl/util/pathx"
	"gitee.com/go-ultra/gouctl/util/stringx"
)

//go:embed etc.tpl
var etcTemplate string

// GenEtc generates the yaml configuration file of the rpc service,
// including host, port monitoring configuration items and etcd configuration
func (g *Generator) GenEtc(ctx DirContext, _ parser.Proto, cfg *conf.Config) error {
	dir := ctx.GetEtc()
	etcFilename, err := format.FileNamingFormat(cfg.NamingFormat, ctx.GetServiceName().Source())
	if err != nil {
		return err
	}

	fileName := filepath.Join(dir.Filename, fmt.Sprintf("%v.yaml", etcFilename))

	text, err := pathx.LoadTemplate(category, etcTemplateFileFile, etcTemplate)
	if err != nil {
		return err
	}

	return util.With("etc").Parse(text).SaveTo(map[string]interface{}{
		"serviceName": strings.ToLower(stringx.From(ctx.GetServiceName().Source()).ToCamel()),
	}, fileName, false)
}
