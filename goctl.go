package main

import (
	"gitee.com/go-ultra/gou/core/load"
	"gitee.com/go-ultra/gou/core/logx"
	"gitee.com/go-ultra/gouctl/cmd"
)

func main() {
	logx.Disable()
	load.Disable()
	cmd.Execute()
}
