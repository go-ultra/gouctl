package builderx

import (
	"gitee.com/go-ultra/gou/core/stores/builder"
)

// Deprecated: Use gitee.com/go-ultra/gou/core/stores/builder.RawFieldNames instead.
func FieldNames(in interface{}) []string {
	return builder.RawFieldNames(in)
}

// Deprecated: Use gitee.com/go-ultra/gou/core/stores/builder.RawFieldNames instead.
func RawFieldNames(in interface{}, postgresSql ...bool) []string {
	return builder.RawFieldNames(in, postgresSql...)
}

// Deprecated: Use gitee.com/go-ultra/gou/core/stores/builderx.PostgreSqlJoin instead.
func PostgreSqlJoin(elems []string) string {
	return builder.PostgreSqlJoin(elems)
}
