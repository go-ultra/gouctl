package template

const (
	// Imports defines a import template for model in cache case
	Imports = `import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	{{if .time}}"time"{{end}}

	"gitee.com/go-ultra/gou/core/stores/builder"
	"gitee.com/go-ultra/gou/core/stores/cache"
	"gitee.com/go-ultra/gou/core/stores/sqlc"
	"gitee.com/go-ultra/gou/core/stores/sqlx"
	"gitee.com/go-ultra/gou/core/stringx"
)
`
	// ImportsNoCache defines a import template for model in normal case
	ImportsNoCache = `import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	{{if .time}}"time"{{end}}

	"gitee.com/go-ultra/gou/core/stores/builder"
	"gitee.com/go-ultra/gou/core/stores/sqlc"
	"gitee.com/go-ultra/gou/core/stores/sqlx"
	"gitee.com/go-ultra/gou/core/stringx"
)
`
)
