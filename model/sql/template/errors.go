package template

// Error defines an error template
const Error = `package {{.pkg}}

import "gitee.com/go-ultra/gou/core/stores/sqlx"

var ErrNotFound = sqlx.ErrNotFound
`
