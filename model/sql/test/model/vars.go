package model

import "gitee.com/go-ultra/gou/core/stores/sqlx"

// ErrNotFound types an alias for sqlx.ErrNotFound
var ErrNotFound = sqlx.ErrNotFound
